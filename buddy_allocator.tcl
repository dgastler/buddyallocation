#################################################################################
## CreateBuddyAllocation
#################################################################################
# Create the top node of this tree
# Args
#   size:  The total size of the range this will allocate in
#   block_size:  The smallest allocation allowed
#   starting_address:  The address of the start of the range
# ret:
#   dict: The top node of the buddy allocation tree
proc CreateBuddyAllocation {size  block_size  starting_address} {
    set order [expr int(floor(log($size/$block_size)/log(2)))] 
    set address_hi [expr int($starting_address + ($block_size * (2**$order)))]
    set BT [dict create \
		size $size \
		block_size $block_size \
		order $order \
		address_lo $starting_address \
		address_hi $address_hi \
		free true \
		left_child "" \
		right_child "" ]
    return $BT
}

#################################################################################
## GetBlockAtAddress
#################################################################################
# Get a block at a specific address
# Args
#   BT: Current buddy allocation tree
#   start_offfset: Start of the range desired
#   end_off: Last address in the desired range
# ret:
#   list:
#     index 0: The beginning address of the block that contains the requested range (-1 if failed)
#     index 1: The updated BT
proc GetBlockAtAddress { BT start_offset end_offset } {
    set ret_addr -1
    dict update BT  size size block_size block_size order order address_lo address_lo address_hi address_hi free free left_child left_child right_child right_child {
	if { $free } {
	    if {$address_lo <= $start_offset && $end_offset < $address_hi} {
		#This allocation is within this Node

		if {$address_lo <= $start_offset &&
		    $end_offset < [expr ($address_lo + $address_hi)/2] } {		    
		    #the range fits in the left child
		    #check if the child exists
		    if { [dict size $left_child] == 0 } {
			#create the missing child
			set left_child [CreateBuddyAllocation \
					[expr int($size/2)] \
					$block_size \
					$address_lo]			
		    }
		    #Pass this range to the child
		    set ret [GetBlockAtAddress $left_child $start_offset $end_offset]
		    if {[lindex $ret 0] != -1} {
			set left_child [lindex $ret 1]
			set ret_addr [lindex $ret 0]
		    }		    
		} elseif {[expr ($address_lo + $address_hi)/2] <= $start_offset &&
			   $end_offset < $address_hi } {
		    #the range fits in the right child
		    #check if the child exists
		    if { [dict size $right_child] == 0 } {
			#create the missing child
			set right_child [CreateBuddyAllocation \
					 [expr int($size/2)] \
					 $block_size \
					 [expr ($address_lo + $address_hi)/2]]			
		    }
		    #pass this range to the child
		    set ret [GetBlockAtAddress $right_child $start_offset $end_offset]
		    if {[lindex $ret 0] != -1} {
			set right_child [lindex $ret 1]
			set ret_addr [lindex $ret 0]
		    }		    
		} else {
		    #this couldn't fit in the children, so we can take it
		    set free false
		    set ret_addr $address_lo
		}
	    } 
	}
    }
    return [list $ret_addr $BT]
}

#################################################################################
## GetBlock
#################################################################################
# Get a block for the requested size
# Args
#   BT: Current buddy allocation tree
#   requested_size: size of block to allocate
# ret:
#   list:
#     index 0: The beginning address of the block allocated (-1 if failed)
#     index 1: The updated BT
proc GetBlock {BT requested_size} {
    set ret_addr -1
    dict update BT size size block_size block_size order order address_lo address_lo address_hi address_hi free free left_child left_child right_child right_child {
	if { $requested_size <= $size && $free} {
	    if {$requested_size < $block_size} {
		set block_order 0
	    } else {
		#get block order
		set block_order [expr int(floor(log($requested_size/$block_size)/log(2)))]
	    }
	    
	    if { $order == $block_order } {
		#This is the right size
		if { [dict size $left_child] == 0 && [dict size $right_child] == 0 } {
		    #this is free, mark it as used and return the address
		    set free false
		    set ret_addr $address_lo		    
		}
	    } else {
		#start on the left
		if {[dict size $left_child] == 0} {
		    # no left child, make it		
		    set left_child [CreateBuddyAllocation [expr int($size/2)] $block_size $address_lo]
		}
		if {[dict get $left_child free]} {		
		    set ret [GetBlock $left_child $requested_size]
		    if {[lindex $ret 0] != -1} {
			set left_child [lindex $ret 1]
			set ret_addr [lindex $ret 0]
		    }
		}

		if {[lindex $ret_addr 0] == -1} {
		    #try the right side
		    if {[dict size $right_child] == 0} {
			# no right child, make it
			set right_child [CreateBuddyAllocation \
					     [expr int($size/2)] \
					     $block_size \
					     [expr $address_lo + int($block_size*(2**($order-1)))]]
		    }
		    if {[dict get $right_child free]} {
			set ret [GetBlock $right_child $requested_size]
			if {[lindex $ret 0] != -1} {
			    set right_child [lindex $ret 1]
			    set ret_addr [lindex $ret 0]
			}
		    }
		}
	    }
	}
    }	    
    return [list $ret_addr $BT]
}

#################################################################################
## ReturnBlock
#################################################################################
# Return a block so it can be re-used
# Args
#   BT: Current buddy allocation tree
#   address: An address in the range of the block to be removed
# ret:
#   list:
#     index 0: The size of data freed (-1 if failed)
#     index 1: The updated BT
proc ReturnBlock {BT address} {
    set ret_val -1
    dict update BT size size block_size block_size order order address_lo address_lo address_hi address_hi free free left_child left_child right_child right_child {
	if { ($address >= $address_lo) && ($address < $address_hi)} {
	    #this is in our address space
	    #do we have children?
	    if { [dict size $left_child] == 0 && [dict size $right_child] == 0 && [expr !$free] } {
		#we have no children and can release this range
		set free true
		set ret_val [expr $address_hi - $address_lo]
	    } else {
		#we have children
		#try left child first
		if {[dict size $left_child] > 0} {
		    set freed [ReturnBlock $left_child $address]
		    if {[lindex $freed 0] >= 0} {
			#check if we can reclaim this
			set left_child [lindex $freed 1]
			if {[dict get $left_child free]} {
			    if { [dict size [dict get $left_child left_child]]  == 0 &&
				 [dict size [dict get $left_child right_child]] == 0 } {
				set left_child ""
			    }
			}
			set ret_val [lindex $freed 0]
		    }
		}

		
		if { $ret_val < 0} {
		    #try right child
		    if {[dict size $right_child] > 0} {
			set freed [ReturnBlock $right_child $address]
			if {[lindex $freed 0] >= 0} {
			    #check if we can reclaim this
			    set right_child [lindex $freed 1]
			    if {[dict get $right_child free]} {
				if { [dict size [dict get $right_child left_child]]  == 0 &&
				     [dict size [dict get $right_child right_child]] == 0 } {
				    set right_child ""
				}
			    }
			    set ret_val [lindex $freed 0]
			}
		    }
		}
	    }
	}
    }
    return [list $ret_val $BT]
}
