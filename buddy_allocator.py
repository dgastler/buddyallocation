import random
import math

class BuddyNode:
    def __init__(self,address,block_size,order):
        self.block_size = block_size
        self.address_lo = address
        self.order = order
        self.address_hi = int(self.address_lo + (self.block_size*(2**self.order)))
        self.free = True
        self.left_child = None
        self.right_child = None

    def GetAllocations(self):
        allocations = []
        if not self.free:
            allocations = allocations + [[self.address_lo,self.address_hi]]
        else:
            if self.left_child != None:
                allocations = allocations + self.left_child.GetAllocations()
            if self.right_child != None:
                allocations = allocations + self.right_child.GetAllocations()
        return allocations
    
    def Print(self,indent=""):

        if self.right_child == None and self.left_child == None and not self.free:
            #print me
            if indent[-1] == "|":
                indent = indent[0:-1] + ' '
            print(indent+"-> 0x"+hex(self.address_lo)[2:].zfill(8)+" size: 0x"+hex(int(self.block_size*(2**self.order)))[2:].zfill(8))
            return
        else:
            print(indent+"\\")
        #do we have a second child 
        if self.right_child != None and self.left_child != None:
            l_indent=indent+"|"
        else:
            l_indent=indent+" "
        r_indent=indent+"|"
            
        #print left first
        if self.left_child != None:
            self.left_child.Print(l_indent)
        if self.right_child != None:
            self.right_child.Print(r_indent)
            
        
    def Remove(self,address):
        if (address >= self.address_lo) and (address < self.address_hi):
            #this is in our address space
            
            #do we have children?
            if self.left_child == None and self.right_child == None and not self.free:
                #we have no children and can release this range
                self.free=True
                return (self.address_hi-self.address_lo)

            #we have children            

            #try left child first
            if self.left_child != None:
                freed = self.left_child.Remove(address)
                if freed >= 0:
                    #check if we can reclaim this
                    if self.left_child.free:
                        if self.left_child.left_child == None and self.left_child.right_child == None:
                            self.left_child = None
                    return freed
            #try right child
            if self.right_child != None:
                freed = self.right_child.Remove(address)
                if freed >= 0:
                    #check if we can reclaim this
                    if self.right_child.free:
                        if self.right_child.left_child == None and self.right_child.right_child == None:
                            self.right_child = None
                    return freed

        return -1

    def AddAtAddress(self, start_offset, end_offset):
        if not self.free:
            return -1
        if self.address_lo <= start_offset and end_offset < self.address_hi:
            #This allocation is within this Node
            #check if this range in the left child's range
            if ((self.address_lo <= start_offset) and
                (end_offset < int((self.address_lo+self.address_hi)/2))):
                #check if this exists
                if self.left_child == None:
                    #this fits in the left so make it
                    self.left_child = BuddyNode(self.address_lo,
                                                self.block_size,
                                                self.order-1)
                return self.left_child.AddAtAddress(start_offset,end_offset)

            #check if this range in the right child's range
            if ((int((self.address_lo+self.address_hi)/2) <= start_offset) and
                (end_offset < self.address_hi)):
                #check if this exists
                if self.right_child == None:
                    #this fits in the right so make it
                    self.right_child = BuddyNode(int(self.address_lo+(self.block_size*(2**(self.order-1)))),
                                                 self.block_size,
                                                 self.order-1)
                return self.right_child.AddAtAddress(start_offset,end_offset)

            #this couldn't fit in any child, so we can take it.
            self.free = False
            return self.address_lo
        return -1
            
    def Add(self,order):
        if not self.free:
            return -1
        if self.order == order:
            #This is the right size
            if self.left_child == None and self.right_child == None and self.free:
                #this is free, mark it as used and return the address
                self.free = False
                return self.address_lo
            else:
                #this entry is full
                return -1
        else:
            #we are too big for this, try our children
            ret_address = -1

            #start on the left
            
            if self.left_child == None:
                # no left child, make it
                self.left_child = BuddyNode(self.address_lo,
                                            self.block_size,
                                            self.order-1)
            if self.left_child.free:
                ret_address = self.left_child.Add(order)            
                if ret_address != -1:
                    return ret_address
            
            #try the right side
            
            if self.right_child == None:
                # no right child, make it
                self.right_child = BuddyNode(int(self.address_lo+(self.block_size*(2**(self.order-1)))),
                                             self.block_size,
                                             self.order-1)
            if self.right_child.free:
                ret_address = self.right_child.Add(order)
                if ret_address != -1:
                    return ret_address

            return ret_address
        #this can't happen
        return -1

            
class BuddyTree:
    def __init__(self,size,block_size):
        self.MIN_SIZE = block_size
        self.SIZE     = size
        self.top      = BuddyNode(0,self.MIN_SIZE,int(math.floor(math.log2(self.SIZE/self.MIN_SIZE))))
    def GetBlockAtAddress(self,start_offset,end_offset):
        size = int(end_offset - start_offset)
        start_offset = int(start_offset)
        end_offset = int(end_offset)
        #check if this could fit in our memory
        if size > self.SIZE:
            return int(-1)
        return self.top.AddAtAddress(start_offset,end_offset)        
    def GetBlock(self,size):
        size = int(size)
        #check if this could fit in our memory
        if size > self.SIZE:
            return int(-1)
        #get block order
        order=int(math.log2(size/self.MIN_SIZE))
        #find a block of this size
        offset=int(self.top.Add(order))
        if offset < 0:
            raise BaseException("allocation failed")
        return offset
    def FreeBlock(self,address):
        removed = self.top.Remove(int(str(address),0))
        if removed < 0:
            raise BaseException("deallocation failed")
        return int(removed)
    def GetAllocations(self):
        return self.top.GetAllocations()
    def Print(self):
        self.top.Print("")


